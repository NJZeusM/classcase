#### 远程创建版本库，本地clone
git clone [远程仓库地址] #克隆一个本地库
git add [文件名] 　#把文件添加到仓库，当一次添加所有有改动的文件时，[文件名]改为[.]
git commit -m “[说明文字]”  #把文件提交到仓库
git push origin master #把文件推送到远程仓库

#### 本地创建版本库
git init  #本地新建文件夹。初始化仓库，这是个Git可以管理的仓库
git remote add origin  [远程仓库地址] #在本地仓库中执行该命令
git add [文件名] 　#把文件添加到仓库，当一次添加所有有改动的文件时，[文件名]改为[.]
git commit -m “[说明文字]”  #把文件提交到仓库
git push [-u] origin master #把文件推送到远程仓库,-u为可选参数，作用是除同步内容，还同步本地分支

#### 关于分支
git checkout -b [自己的分支名] #切换分支，若分支不存在，新建并切换
git add . 把自己分支修改的内容提交到暂存区（Stage）
git commit -m "*" 一次性把暂存区的所有修改提交到分支
git checkout master
git pull origin master
git checkout <your-branch-name>
git merge master (若出现冲突，需先回退本地版本，然后人工合并)
git commit -m "*"
git push origin <your-branch-name>

进入项目分支页，点击个人分支旁的“合并请求”
